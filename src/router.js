import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

export default new Router({
  mode: "history",
  routes: [
    {
      path: "/",
      name: "login",
      component: () => import("./components/Login"),
    },
    {
      path: "/tutorials",
      // alias: "/tutorials",
      name: "tutorials",
      component: () => import("./components/TutorialsList")
    },
    {
      path: "/tutorials/:id",
      name: "tutorial-details",
      component: () => import("./components/Tutorial")
    },
    {
      path: "/logs",
      name: "logs",
      component: () => import("./components/LogsList")
    },
    {
      path: "/logsDescription",
      name: "logDescription",
      component: () => import("./components/LogDescription")
    },
    {
      path: "/customers",
      name: "customers",
      component: () => import("./components/Customer")
    },
    {
      path: "/logsDescription/:id",
      name: "logDescription-id",
      component: () => import("./components/LogDescription")
    },
    {
      path: "/question",
      name: "question",
      component: () => import("./components/Question")
    },
    {
      path: "/questiondescription",
      name: "questiondescription-id",
      component: () => import("./components/QuestionDescription")
    },
  ]
});

import Vue from 'vue'
import App from './App.vue'
import { BootstrapVue } from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import router from './router'
import vuetify from './plugins/vuetify';
import "./assets/styles.scss";
import VueLocalStorage from 'vue-localstorage'


Vue.config.productionTip = false
Vue.use(VueLocalStorage)
Vue.use(VueLocalStorage, {
  name: 'ls',
  bind: true //created computed members from your variable declarations
})
Vue.use(BootstrapVue)

new Vue({
  router,
  vuetify,
  render: h => h(App),
}).$mount('#app')

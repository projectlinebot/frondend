import http from '../http-common'


class LogDataService {

  getAll(params) {
    return http.get("/logs", { params });
  }

  get(user) {
    return http.get(`/logs/${user}`);
  }

  create(data) {
    return http.post("/logs", data);
  }

  findByTitle(user) {
    return http.get(`/logs?user=${user}`);
  }
}

export default new LogDataService();

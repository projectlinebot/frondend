import http from "../http-common";

class QuestionDataService {
  getAll(params) {
    return http.get("/question", { params });
  }

  get(userid) {
    return http.get(`/question/${userid}`);
  }

  findByTitle(userid) {
    return http.get(`/question?user=${userid}`);
  }
}

export default new QuestionDataService();

import http from "../http-common";

class ResultDataService {
  getAll(params) {
    return http.get("/result", { params });
  }

  get(userid) {
    return http.get(`/result/${userid}`);
  }

  findByTitle(userid) {
    return http.get(`/result?user=${userid}`);
  }
}

export default new ResultDataService();
